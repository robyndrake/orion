#+TITLE: TODO for Orion

* Kernel
** VGA
*** DONE Newline support
*** TODO Implement terminal scrolling
    Move all lines up one row and leave blank row at the bottom
*** TODO Colors changing (for ASCII arts)
   * [ ] Add better VGA driver (to display colors)      
** TODO Improve Build
   * [ ] Add arguments 
** TODO Global Constructors Table
   * [X] Init GDT in early kernel
   * [ ] Add LDT (Local Descriptor Table)
** TODO Serial
   * [ ] Print debuging messages to Serial
   * [ ] Add more arguments to serial_printf function
   * [ ] Add input from Serial
   * [ ] Add debug hexdump function
   * [ ] Add printing to serial with registers state 
** TODO Interrupt Description Table
   * [ ] Init IDT in early kernel
   * [ ] Add Some Exceptions
** TODO Memory
   * [ ] Paging
   * [ ] Permissions
   * [ ] Memory Map

* LibC  
** TODO Add some more arguments (decorators, codes) for printf
  * [ ] %c :: character value
  * [ ] %s :: string of characters
  * [ ] %d :: signed integer
  * [ ] %i :: signed integer
  * [ ] %f :: floating point value
  * [ ] %e :: scientific notation, with a lowercase “e”
  * [ ] %E :: scientific notation, with a uppercase “E”
  * [ ] %g :: use %e or %f
  * [ ] %G :: use %E or %f
  * [ ] %o :: octal
  * [ ] %u :: unsigned integer
  * [ ] %x :: unsigned hexadecimal, with lowercase letters
  * [ ] %X :: unsigned hexadecimal, with uppercase letters
  * [ ] %p :: a pointer
  * [ ] %n :: the argument shall be a pointer to an integer in which the number of characters written is placed
  * [ ] %% :: shall be a % sign
** TODO Memory
  * [ ] malloc
  * [ ] free
