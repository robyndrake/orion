#define IRQ0 32
#define IRQ1 33
#define IRQ2 34
#define IRQ3 35
#define IRQ4 36
#define IRQ5 37
#define IRQ6 38
#define IRQ7 39
#define IRQ8 40
#define IRQ9 41
#define IRQ10 42
#define IRQ11 43
#define IRQ12 44
#define IRQ13 45
#define IRQ14 46
#define IRQ15 47

//#include "asm_helper.h"

struct idt_entry_struct
{
	uint16_t    base_lo;      // The lower 16 bits of the ISR's address
	uint16_t    sel;    // The GDT segment selector that the CPU will load into CS before calling the ISR
	uint8_t     always0;     // Always set to zero
	uint8_t     flags;   // Type and attributes
	uint16_t    base_hi;     // The higher 16 bits of the ISR's address
} __attribute__((packed));
typedef struct idt_entry_struct idt_entry_t;

struct idt_ptr_struct {
	uint16_t	limit;
	uint32_t	base;
} __attribute__((packed));
typedef struct idt_ptr_struct idt_ptr_t;

idt_entry_t idt_entries[256];
idt_ptr_t idt_ptr;

typedef struct registers
{
   uint32_t ds;                  // Data segment selector
   uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; // Pushed by pusha.
   uint32_t int_no, err_code;    // Interrupt number and error code (if applicable)
   uint32_t eip, cs, eflags, useresp, ss; // Pushed by the processor automatically.
} registers_t;

typedef void (*isr_t)(registers_t);
isr_t interrupt_handlers[256];

void idt_set_gate(uint8_t num, uint32_t base, uint16_t sel, uint8_t flags) {
  idt_entries[num].base_lo = base & 0xFFFF;
  idt_entries[num].base_hi = (base >> 16) & 0xFFFF;

  idt_entries[num].sel = sel;
  idt_entries[num].always0 = 0;

  idt_entries[num].flags = flags;
}

void idt_init(void);
void idt_init() {
  idt_ptr.limit = sizeof(idt_entry_t) * 256 - 1;
  idt_ptr.base = (uint32_t)idt_entries;

  memset(idt_entries, 0, sizeof(idt_entry_t)*256);

  extern isr_t isr0;
  extern isr_t isr1;
  extern isr_t isr2;
  extern isr_t isr3;
  extern isr_t isr4;
  extern isr_t isr5;
  extern isr_t isr6;
  extern isr_t isr7;
  extern isr_t isr8;
  extern isr_t isr9;
  extern isr_t isr10;
  extern isr_t isr11;
  extern isr_t isr12;
  extern isr_t isr13;
  extern isr_t isr14;
  extern isr_t isr15;
  extern isr_t isr16;
  extern isr_t isr17;
  extern isr_t isr18;
  extern isr_t isr19;
  extern isr_t isr20;
  extern isr_t isr21;
  extern isr_t isr22;
  extern isr_t isr23;
  extern isr_t isr24;
  extern isr_t isr25;
  extern isr_t isr26;
  extern isr_t isr27;
  extern isr_t isr28;
  extern isr_t isr29;
  extern isr_t isr30;
  extern isr_t isr31;

  extern isr_t irq0;
  extern isr_t irq1;
  extern isr_t irq2;
  extern isr_t irq3;
  extern isr_t irq4;
  extern isr_t irq5;
  extern isr_t irq6;
  extern isr_t irq7;
  extern isr_t irq8;
  extern isr_t irq9;
  extern isr_t irq10;
  extern isr_t irq11;
  extern isr_t irq12;
  extern isr_t irq13;
  extern isr_t irq14;
  extern isr_t irq15;
  
  idt_set_gate(0, (uint32_t)&isr0, 0x08, 0x8E);
  idt_set_gate(1, (uint32_t)&isr1, 0x08, 0x8E);
  idt_set_gate(2, (uint32_t)&isr2, 0x08, 0x8E);
  idt_set_gate(3, (uint32_t)&isr3, 0x08, 0x8E);
  idt_set_gate(4, (uint32_t)&isr4, 0x08, 0x8E);
  idt_set_gate(5, (uint32_t)&isr5, 0x08, 0x8E);
  idt_set_gate(6, (uint32_t)&isr6, 0x08, 0x8E);
  idt_set_gate(7, (uint32_t)&isr7, 0x08, 0x8E);
  idt_set_gate(8, (uint32_t)&isr8, 0x08, 0x8E);
  idt_set_gate(9, (uint32_t)&isr9, 0x08, 0x8E);
  idt_set_gate(10, (uint32_t)&isr10, 0x08, 0x8E);
  idt_set_gate(11, (uint32_t)&isr11, 0x08, 0x8E);
  idt_set_gate(12, (uint32_t)&isr12, 0x08, 0x8E);
  idt_set_gate(13, (uint32_t)&isr13, 0x08, 0x8E);
  idt_set_gate(14, (uint32_t)&isr14, 0x08, 0x8E);
  idt_set_gate(15, (uint32_t)&isr15, 0x08, 0x8E);
  idt_set_gate(16, (uint32_t)&isr16, 0x08, 0x8E);
  idt_set_gate(17, (uint32_t)&isr17, 0x08, 0x8E);
  idt_set_gate(18, (uint32_t)&isr18, 0x08, 0x8E);
  idt_set_gate(19, (uint32_t)&isr19, 0x08, 0x8E);
  idt_set_gate(20, (uint32_t)&isr20, 0x08, 0x8E);
  idt_set_gate(21, (uint32_t)&isr21, 0x08, 0x8E);
  idt_set_gate(22, (uint32_t)&isr22, 0x08, 0x8E);
  idt_set_gate(23, (uint32_t)&isr23, 0x08, 0x8E);
  idt_set_gate(24, (uint32_t)&isr24, 0x08, 0x8E);
  idt_set_gate(25, (uint32_t)&isr25, 0x08, 0x8E);
  idt_set_gate(26, (uint32_t)&isr26, 0x08, 0x8E);
  idt_set_gate(27, (uint32_t)&isr27, 0x08, 0x8E);
  idt_set_gate(28, (uint32_t)&isr28, 0x08, 0x8E);
  idt_set_gate(29, (uint32_t)&isr29, 0x08, 0x8E);
  idt_set_gate(30, (uint32_t)&isr30, 0x08, 0x8E);
  idt_set_gate(31, (uint32_t)&isr31, 0x08, 0x8E);
 
  extern void idt_flush(uint32_t);
  idt_flush((uint32_t)&idt_ptr);

  // Remap the irq table.
  outb(0x20, 0x11);
  outb(0xA0, 0x11);
  outb(0x21, 0x20);
  outb(0xA1, 0x28);
  outb(0x21, 0x04);
  outb(0xA1, 0x02);
  outb(0x21, 0x01);
  outb(0xA1, 0x01);
  outb(0x21, 0x0);
  outb(0xA1, 0x0);
  
  idt_set_gate(32, (uint32_t)&irq0, 0x08, 0x8E);
  idt_set_gate(33, (uint32_t)&irq1, 0x08, 0x8E);
  idt_set_gate(34, (uint32_t)&irq2, 0x08, 0x8E);
  idt_set_gate(35, (uint32_t)&irq3, 0x08, 0x8E);
  idt_set_gate(36, (uint32_t)&irq4, 0x08, 0x8E);
  idt_set_gate(37, (uint32_t)&irq5, 0x08, 0x8E);
  idt_set_gate(38, (uint32_t)&irq6, 0x08, 0x8E);
  idt_set_gate(39, (uint32_t)&irq7, 0x08, 0x8E);
  idt_set_gate(40, (uint32_t)&irq8, 0x08, 0x8E);
  idt_set_gate(41, (uint32_t)&irq9, 0x08, 0x8E);
  idt_set_gate(42, (uint32_t)&irq10, 0x08, 0x8E);
  idt_set_gate(43, (uint32_t)&irq11, 0x08, 0x8E);
  idt_set_gate(44, (uint32_t)&irq12, 0x08, 0x8E);
  idt_set_gate(45, (uint32_t)&irq13, 0x08, 0x8E);
  idt_set_gate(46, (uint32_t)&irq14, 0x08, 0x8E);
  idt_set_gate(47, (uint32_t)&irq15, 0x08, 0x8E);

}

void isr_handler(registers_t regs)
{
  if (interrupt_handlers[regs.int_no] != 0)
  {
        isr_t handler = interrupt_handlers[regs.int_no];
        handler(regs);
  } else {
	  //printf("Unhandled interrupt: %c\n", regs.int_no); // FIXME: Fix from %s to %X
	  //serial_printf("Unhandled interrupt: %c\n", regs.int_no); // FIXME: Fix from %s to %X
  }
}

void irq_handler(registers_t regs)
{
   // Send an EOI (end of interrupt) signal to the PICs.
   // If this interrupt involved the slave.
   if (regs.int_no >= 40)
   {
       // Send reset signal to slave.
       outb(0xA0, 0x20);
   }
   // Send reset signal to master. (As well as slave, if necessary).
   outb(0x20, 0x20);

   if (interrupt_handlers[regs.int_no] != 0)
   {
       isr_t handler = interrupt_handlers[regs.int_no];
       handler(regs);
   } else {
   	//serial_printf("Unhandled IRQ interrupt: %c\n", regs.int_no); // FIXME: Fix from %s to %X
   }
}

void register_interrupt_handler(uint8_t n, isr_t handler)
{
  interrupt_handlers[n] = handler;
}
