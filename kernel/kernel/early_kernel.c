#include <stdint.h>
#include <assert.h>

#include <kernel/tty.h>
#include <stdio.h>
#include "serial.h"
#include "gdt.c"
#include "interrupts.c"
//#include "timer.c"
#include "keyboard.c"
#include "paging.h"
//#include "fs.c"
#include "initrd.c"
#include "multiboot.h"

extern heap_t *kheap;
extern fs_node_t *fs_root; // The root of the filesystem.

void kernel_early_main(struct multiboot *mboot_ptr) {
  gdt_init();
  init_serial();
  serial_printf("gdt initialized!\n");
  idt_init();
  serial_printf("idt initialized!\n");
  //init_timer(50); // Initialise timer to 50Hz
  //serial_printf("timer initialized!\n");
  
  // Find the location of our initial ramdisk.
  //assert(mboot_ptr->mods_count > 0);
  if (!(mboot_ptr->mods_count > 0)) {
	  serial_printf("PANIC!!");
  }
  uint32_t initrd_location = *((uint32_t*)mboot_ptr->mods_addr);
  uint32_t initrd_end = *(uint32_t*)(mboot_ptr->mods_addr+4);
  // Don't trample our module with placement accesses, please!
  placement_address = initrd_end;

  init_paging(); 
  serial_printf("paging initialized!\n");
  //open_fs(fs_root, 1, 0);
  fs_root = init_initrd(initrd_location);
  serial_printf("initrd!\n");
  init_keyboard();
  serial_printf("keyboard initialized!\n");
  terminal_initialize(); // FIXME
  int a = kmalloc(23); // FIXME
  free(a, kheap);
}
