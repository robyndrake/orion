#include <stdio.h>
#include <stdint.h>
#include <assert.h>
//#include "fs.c"
//#include "initrd.c"


typedef uint32_t (*read_type_t)(struct fs_node*,uint32_t,uint32_t,uint8_t*);
typedef uint32_t (*write_type_t)(struct fs_node*,uint32_t,uint32_t,uint8_t*);
typedef void (*open_type_t)(struct fs_node*);
typedef void (*close_type_t)(struct fs_node*);
typedef struct dirent * (*readdir_type_t)(struct fs_node*,uint32_t);
typedef struct fs_node * (*finddir_type_t)(struct fs_node*,char *name);


typedef struct fs_node
{
   char name[128];     // The filename.
   uint32_t mask;        // The permissions mask.
   uint32_t uid;         // The owning user.
   uint32_t gid;         // The owning group.
   uint32_t flags;       // Includes the node type. See #defines above.
   uint32_t inode;       // This is device-specific - provides a way for a filesystem to identify files.
   uint32_t length;      // Size of the file, in bytes.
   uint32_t impl;        // An implementation-defined number.
   read_type_t read;
   write_type_t write;
   open_type_t open;
   close_type_t close;
   readdir_type_t readdir;
   finddir_type_t finddir;
   struct fs_node *ptr; // Used by mountpoints and symlinks.
} fs_node_t;

struct dirent // One of these is returned by the readdir call, according to POSIX.
{
  char name[128]; // Filename.
  uint32_t ino;     // Inode number. Required by POSIX.
};

extern fs_node_t *fs_root; // The root of the filesystem.

void kernel_main(void) {
	printf("Hello from OrionOS!\n");
	printf("Testing serial\n");
	serial_printf("Test is success!\n");
	serial_printf("Newline test\n");
	char * test_string = "test";
	serial_printf("This is string from variable: %s\n", test_string);
	printf("Test finished success!\n");
  	asm volatile("sti");

	// list the contents of /
	int i = 0;
	struct dirent *node = 0;
	while ( (node = readdir_fs(fs_root, i)) != 0)
	{
	  serial_printf("Found file %s", node->name);
	  fs_node_t *fsnode = finddir_fs(fs_root, node->name);

	  if ((fsnode->flags&0x7) == 0x02)//FS_DIRECTORY)
	    serial_printf("\n\t(directory)\n");
	  else
	  {
	    printf("\n\t contents: \"");
 	    char buf[256];
	    uint32_t sz = read_fs(fsnode, 0, 256, buf);
	    uint32_t j;
	    for (j = 0; j < sz; j++)
	      serial_printf("%c", buf[j]);

    	    serial_printf("\"\n");
  	  }
	  i++;
	}
	//printf("%x\n", 0x0);
	//printf("%x\n", a);
	printf("%x");	
	/*for (int i = 0; i < 1000; i++) {
		printf("A: %c\n", i);
	}*/
	close_fs(fs_root);
	for(;;) {
		asm("hlt");
	}
}
