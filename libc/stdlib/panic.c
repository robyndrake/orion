#include <stdio.h>
#include <stdlib.h>

__attribute__((__noreturn__))
void panic(char *panic_msg) {
#if defined(__is_libk)
	// TODO: Add proper kernel panic.
  printf("kernel: panic: %s\n", panic_msg);
#endif
	while (1) { }
	__builtin_unreachable();
}
